import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'exp',
    loadChildren: () => import('./exp/exp.module').then( m => m.ExpPageModule)
  },
  {
    path: 'noconformidades', loadChildren: () => import('./noconformidades/noconformidades.module').then( m => m.NoconformidadesPageModule)},
  { path: 'selnoc', loadChildren: () => import('./selnoc/selnoc.module').then( m => m.SelnocPageModule)},
  { path: 'login', loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)},
  
  
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
