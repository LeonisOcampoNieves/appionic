import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NoconformidadesPage } from './noconformidades.page';

describe('NoconformidadesPage', () => {
  let component: NoconformidadesPage;
  let fixture: ComponentFixture<NoconformidadesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoconformidadesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NoconformidadesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
