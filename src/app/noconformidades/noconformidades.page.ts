import { Component, OnInit } from '@angular/core';
import { CrudService } from './../service/crud.service';
 
@Component({
  selector: 'app-noconformidades',
  templateUrl: './noconformidades.page.html',
  styleUrls: ['./noconformidades.page.scss'],
})
export class NoconformidadesPage implements OnInit {

  noconformidad:any;
  noconformidadName: string;
  equipo:any;
  equipoName1:string;
  area:any;
  areaName2:string;
  dsec:any;
  descName0:string;
  

  constructor(private crudService: CrudService) { }

  ngOnInit() {
    this.crudService.read_Newnoconformidades().subscribe(data => {
 
      this.noconformidad = data.map(e => {
        return {
          id: e.payload.doc.id,
          isEdit: false,
          Name: e.payload.doc.data()['Name'],
          Name1: e.payload.doc.data()['Name1'],
          Name2: e.payload.doc.data()['Name2'],
          Name0: e.payload.doc.data()['Name0'],
        };
      })
      console.log(this.noconformidad);
    });
  }

CreateRecord() {
    let record = {};
    record['Name'] = this.noconformidadName;
    record['Name1'] = this.equipoName1;
    record['Name2'] = this.areaName2;
   // record['Name0'] = this.descName0;
    this.crudService.create_Newnoconformidades(record).then(resp => {
      this.noconformidadName= "";
      this.equipoName1 = "";
      this.areaName2 = "";
      this.descName0 = "";
      console.log(resp);
    })
      .catch(error => {
        console.log(error);
      });
  }
 
  RemoveRecord(rowID) {
    this.crudService.delete_Newnoconformidades(rowID);
  }
 
  EditRecord(record) {
    record.isEdit = true;
    record.EditName = record.Name;
    record.EditName1 = record.Name1;
    record.EditName2 = record.Name2;
    //record.EditName0 = record.Name0;
    
  }
 
  UpdateRecord(recordRow) {
    let record = {};
    record['Name'] = recordRow.EditName;
    record['Name1'] = recordRow.EditName1;
    record['Name2'] = recordRow.EditName2;
   // record['Name0'] = recordRow.EditName0;
    this.crudService.update_Newnoconformidades(recordRow.id, record);
    recordRow.isEdit = false;
  }
 
 
}