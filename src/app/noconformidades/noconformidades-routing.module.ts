import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NoconformidadesPage } from './noconformidades.page';

const routes: Routes = [
  {
    path: '',
    component: NoconformidadesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NoconformidadesPageRoutingModule {}
