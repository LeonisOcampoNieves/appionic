import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

//import { NoconformidadesPageRoutingModule } from './noconformidades-routing.module';

import { NoconformidadesPage } from './noconformidades.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    //NoconformidadesPageRoutingModule
    RouterModule.forChild([
      {
        path: '',
        component: NoconformidadesPage
      }
    ])
  ],
  declarations: [NoconformidadesPage]
})
export class NoconformidadesPageModule {}
