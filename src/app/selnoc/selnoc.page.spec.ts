import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelnocPage } from './selnoc.page';

describe('SelnocPage', () => {
  let component: SelnocPage;
  let fixture: ComponentFixture<SelnocPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelnocPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelnocPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
