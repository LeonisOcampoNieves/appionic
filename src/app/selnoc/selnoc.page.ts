import { Component, OnInit } from '@angular/core';
import { CrudService } from './../service/crud.service';
 
@Component({
  selector: 'app-selnoc',
  templateUrl: './selnoc.page.html',
  styleUrls: ['./selnoc.page.scss'],
})
export class SelnocPage implements OnInit {
 
  descripcion = "";
  criterio = "";
  referencia = "";
  riesgo = "";
  noConformidadSeleccionada: any;
  equipos:any;
  ubicacion:any;
  newnoconformidad:any;
  newnoconformidadName3: string;
  newequipo:any;
  newequipoName4:string;
  newarea:any;
  newareaName5:string;

  constructor(private crudService: CrudService ) { }

  ngOnInit() {
    this.crudService.read_Equipos().subscribe(data => {
 
      this.equipos = data.map(e => {
        return {
          id     : e.payload.doc.id,
          isEdit : false,
          Equipo : e.payload.doc.data()['Eq']
        };
      })
    });

    this.crudService.read_Ubicacion().subscribe(data => {
 
      this.ubicacion = data.map(e => {
        return {
          id        : e.payload.doc.id,
          isEdit    : false,
          Ubicacion : e.payload.doc.data()['Ub']
        };
      })
    });

    this.crudService.read_Newnoconformidades().subscribe(data => {
 
      this.newnoconformidad = data.map(e => {
        return {
          id            : e.payload.doc.id,
          isEdit        : false,
          NoConformidad : e.payload.doc.data()['No conformidad'],
          Descripcion   : e.payload.doc.data()['Descripción'],
          Criterio      : e.payload.doc.data()['Criterio'],
          Referencia    : e.payload.doc.data()['Referencia Normativa'],
          Riesgo        : e.payload.doc.data()['Riesgo eléctrico']
        };
      })
      //console.log(this.newnoconformidad);
    });
  }

  onSelectChange() {
    //this.noConformidad.Descripcion = this.noConformidadSeleccionada.Descripcion;

    this.criterio = this.noConformidadSeleccionada.Criterio;
    this.descripcion = this.noConformidadSeleccionada.Descripcion;
    this.referencia = this.noConformidadSeleccionada.Referencia;
    this.riesgo = this.noConformidadSeleccionada.Riesgo;
  }
}