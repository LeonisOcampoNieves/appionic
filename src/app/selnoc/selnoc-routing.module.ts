import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelnocPage } from './selnoc.page';

const routes: Routes = [
  {
    path: '',
    component: SelnocPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelnocPageRoutingModule {}
