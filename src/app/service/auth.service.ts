import { Injectable }       from '@angular/core';
import { promise }          from 'protractor';
import { Router }           from "@angular/router";
import { AngularFirestore } from "@angular/fire/firestore";
import * as firebase        from 'firebase/app';
import 'firebase/auth';

// FireBase
import { AngularFireAuth } from "@angular/fire/auth";

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(private AFauth : AngularFireAuth, private router : Router, private db : AngularFirestore) { }

  login(email: string, password: string) {
    return new Promise((resolve, rejected) => {
      this.AFauth.auth.signInWithEmailAndPassword(email, password).then(user => {
        resolve(user);
      }).catch(err => rejected(err));
    });
  }

  logout(){
    this.AFauth.auth.signOut().then(() => {
      this.router.navigate(['/home']);
    })
  }
}