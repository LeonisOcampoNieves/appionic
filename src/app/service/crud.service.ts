import { Injectable } from '@angular/core';
 
import { AngularFirestore } from '@angular/fire/firestore';
 
@Injectable({
  providedIn: 'root'
})
export class CrudService {
 
  constructor(
    private firestore: AngularFirestore
  ) { }
 
 
  create_NewStudent(record) {
    return this.firestore.collection('Students').add(record);
  }
 
  read_Students() {
    return this.firestore.collection('Students').snapshotChanges();
  }

  read_Equipos() {
    return this.firestore.collection('Equipos').snapshotChanges();
  }

  read_Ubicacion() {
    return this.firestore.collection('Ubicacion').snapshotChanges();
  }

  read_Newnoconformidades() {
    return this.firestore.collection('Newnoconformidades').snapshotChanges();
  }

  /*read_NewnoconformidadesById(Id) {
    return this.firestore.collection('Newnoconformidades').doc(Id).snapshotChanges();
  }*/
 
  update_Student(recordID,record){
    this.firestore.doc('Students/' + recordID).update(record);
  }
 
  delete_Student(record_id) {
    this.firestore.doc('Students/' + record_id).delete();
  }

create_Newnoconformidades(record) {
  return this.firestore.collection('Newnoconformidades').add(record);
}

update_Newnoconformidades(recordID,record){
  this.firestore.doc('Newnoconformidades/' + recordID).update(record);
}

delete_Newnoconformidades(record_id) {
  this.firestore.doc('Newnoconformidades/' + record_id).delete();
}


read_Selnoc() {
  return this.firestore.collection('Newnoconformidades').snapshotChanges();
}




}
